# Hash Pointers

Hash pointers point to a data item (like a normal pointer) but also contain a hash of the data itself, providing an integrity test of the data.

![Hash Pointer](images/hash-pointer.png)

## Blockchain

Blockchain is a a linked list built using hash pointers.

![Hash Pointer](images/blockchain.png)

It is tamper evident because if we make a change to the data in say a block, it will be evident by examining the hash of the previous blocks. Any adversary tampering with a data item must alter every single data point after that item.

## Merkle Trees

A Merkle Tree is a binary search tree built using hash pointers. (Variant: Sorted Merkle Tree)

![Merkle Tree](images/merkle-tree.png)

* Proof of membership (Merkle Tree): O(log (n))
* Proof of non-membership (Sorted Merkle Tree): O(log(n))

![Merkle Tree Membership](images/merkle-tree-member.png)

## General Case

We can use hash pointers for any acyclic pointer based data structure.