# Digital Signatures

A digital signature must:

1. Allow specific user to sign but everyone to verify.
2. Be tied to a specific document. Should be impossible to cut or copy it.

## Digital Signature Scheme

* (secretKey, publicKey) := generateKey(keySize) [Should use randomized algorithms]
* sig := sign(secretKey, publicKey) [Should use randomized algorithms, esp in ECDSA]
* isValid := verify(publicKey, sig)

## Requirements

1. Valid signatures verify
2. Impossible to forge signature

Bitcoin uses a particular digital signature scheme that’s called the Elliptic Curve Digital Signature Algorithm (ECDSA). This is just a quirk of Bitcoin, as this was chosen by Satoshi in the early specification of the system and is now difficult to change.

More specifically,  Bitcoin uses ECDSA over the standard elliptic curve “secp256k1” which is estimated to provide 128 bits of security (that is, it is as difficult to break this algorithm as performing $2^{128}$  symmetric‐key cryptographic operations such as invoking a hash function.

**With ECDSA, a good source of randomness is essential because a bad source of randomness will likely leak your key.**