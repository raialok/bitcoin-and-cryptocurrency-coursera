# Cryptographic  Hash Functions

## Properties

1. Input can be of any size.
2. Fixed size output (e.g. 256 bits).
3. Efficiently computable (runtime O(n))

## Properties of Cryptograhically Secure Hash Function

1. Collision Resistance - Infeasible to find x & y such that H(x) == H(y).
2. Hiding Property - Given H(nonce || x), infeasible to find x.
    * Commit(msg, nonce) = Commitment
    * Verify(msg, nonce, commitment)
    * Hiding: Given H(nonce ‖ msg), it is infeasible to find msg
    * Binding: It is infeasible to find two pairs ( msg, nonce) and (msg’, nonce’) such that msg ≠ msg’ and H(n once ‖ msg) == H(n once’ ‖ msg’).
3. Puzzle friendliness - Given H(k||x), it is infeasible to find x in time less than $2^n$.

Bitcoin uses SHA-256 hash function extensively

![SHA-256](images/sha.png)