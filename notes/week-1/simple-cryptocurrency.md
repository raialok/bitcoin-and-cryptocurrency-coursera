# Simple Cryptocurrency

## GoofyCoin

GoofyCoin has the following rules:

1. Goofy generates a unique coin ID (uniqueCoinID), not previously generated. He constructs the string CreateCoin(uniqueCoinID). He then computes the digital signature of this string with his unique signing key. **The string along with signature is a coin.**

2. Whoever owns a coin can pass it on to someone else by signing a statement that saying, “Pass on this coin to X” (where X is specified as a public key).

3. Anyone can verify the validity of a coin by following the chain of hash pointers back to its creation by Goofy, verifying all of the signatures along the way.

GoofyCoin is vulnerable to double spending attack.

**Double Spending attack is the primary design challenge in cryptocurrencies.**

![GoofyCoin Blockchain](images/goofy.png)

## ScroogeCoin

1. ScroogeCoin is similar to GoofyCoin with added property that Scrooge publishes an append-only, publicly verifiable, ledger.

2. Scrooge verifies and signs all valid transactions.

![ScroogeCoin Blockchain](images/scroogeCoin-blockchain.png)

In ScroogeCoin there are two types of transactions:

1. CreateCoin - Allows creation of multiple coins at once. ![CreateCoin](images/createCoin.png)

2. PayCoins - It consumes some coins, that is, destroys them, and creates new coins of the same total value. The new coins might belong to different people (public keys). PayCoins transaction is valid if four things are true:
    * The consumed coins are valid, that is, they really were created in previous transactions.
    * The consumed coins were not already consumed in some previous transaction. That is, that
    this is not a double‐spend.
    * The total value of the coins that come out of this transaction is equal to the total value of the coins that went in. That is, only Scrooge can create new value.
    * The transaction is validly signed by the owners of all of the consumed coins.

![PayCoin](images/payCoins.png)

*ScroogeCoin has excessive centralization and is dependent on Scrooge's integrity and competence.*